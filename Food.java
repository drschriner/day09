public class Food
{
    private String Description;
    
    
    /**
     * The getter or accesor for the description member
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * The setter or mutator for the description member
     * @param inDescription the new description
     */
    public void setDescription(String inDescription)
    {
        this.description = inDescription;
    }
    public String description;
}